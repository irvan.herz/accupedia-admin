import { ConfigProvider, theme } from 'antd'
import { DashboardPage } from 'pages/dashboard'
import { OrderListPage } from 'pages/order-list'
import { ProductCategoryListPage } from 'pages/product-category-list'
import { ProductDetailsPage } from 'pages/product-details'
import { ProductListPage } from 'pages/product-list'
import { ProfilePage } from 'pages/profile'
import { SigninPage } from 'pages/signin'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { AccuratePage } from './pages/accurate'
import { HomePage } from './pages/home'
import { LogsPage } from './pages/logs'
import { NotFoundPage } from './pages/not-found'
import { TokopediaPage } from './pages/tokopedia'

const router = createBrowserRouter([
  {
    id: '/',
    path: '/',
    element: <HomePage />
  },
  {
    id: '/dashboard',
    path: '/dashboard',
    element: <DashboardPage />
  },
  {
    id: '/products/categories',
    path: '/products/categories',
    element: <ProductCategoryListPage />
  },
  {
    id: '/products',
    path: '/products',
    element: <ProductListPage />
  },
  {
    id: '/products/:id',
    path: '/products/:id',
    element: <ProductDetailsPage />
  },
  {
    id: '/orders',
    path: '/orders',
    element: <OrderListPage />
  },
  {
    id: '/profile',
    path: '/profile',
    element: <ProfilePage />
  },
  {
    id: '/accurate',
    path: '/accurate',
    element: <AccuratePage />
  },
  {
    id: '/tokopedia',
    path: '/tokopedia',
    element: <TokopediaPage />
  },
  {
    id: '/logs',
    path: '/logs',
    element: <LogsPage />
  },
  {
    id: '/auth/signin',
    path: '/auth/signin',
    element: <SigninPage />
  },
  {
    id: '*',
    path: '*',
    element: <NotFoundPage />
  }
])
function App () {
  return (
    <div className="App">
      <ConfigProvider theme={{ algorithm: theme.defaultAlgorithm }}>
        <RouterProvider router={router} />
      </ConfigProvider>
    </div>
  )
}

export default App
