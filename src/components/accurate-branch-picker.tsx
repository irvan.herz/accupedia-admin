import { Button, Input, InputProps, List, Modal, Pagination, Space } from 'antd'
import { ReactElement, cloneElement, useState } from 'react'
import { useQuery } from 'react-query'
import { styled } from 'styled-components'
import { useCustomComponent } from '../hooks/use-custom-component'
import { AccurateService } from '../services/accurate'

type SelectBranchButtonProps = {
  children: ReactElement
  accurate: any
  onSelected: (v: string) => void
}
function SelectBranchButton({ accurate, children, onSelected }: SelectBranchButtonProps) {
  const [open, setOpen] = useState(false)
  const [q, setQ] = useState('')
  const [page, setPage] = useState(1)
  const { data, isLoading } = useQuery(`accurate.branches[${q}${page}]`, () => AccurateService.findManyBranches(accurate.id, q, page), { enabled: open && !!accurate })
  const branches: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleSelect = (b: any) => {
    onSelected(b.no)
    handleClose()
  }
  return (
    <>
      {cloneElement(children, { onClick: handleClick })}
      <Modal
        title="Pilih Branch"
        destroyOnClose
        open={open}
        centered
        footer={null}
        onCancel={handleClose}
      >
        <Input.Search
          onSearch={q => {
            setQ(q)
            setPage(1)
          }}
        />
        <List
          dataSource={branches}
          loading={isLoading}
          renderItem={b => (
            <List.Item extra={<Button disabled={!accurate} onClick={() => handleSelect(b)}>Pilih</Button>}>
              <List.Item.Meta
                title={<Space><div>{b.name}</div><div>({b.no})</div></Space>}
                description={b.no}
              />
            </List.Item>
          )}
          footer={
            <Pagination
              pageSize={1}
              total={meta?.pageCount || 1}
              current={meta?.page || 1}
              onChange={p => setPage(p)}
            />
          }
        />
      </Modal>
    </>
  )
}

const Container = styled.div`
display: flex;
gap: 4px;
.s-1 {
  flex: 1;
}
.s-2 {
  flex: 0;
}
`

type AccurateBranchPickerProps = Omit<InputProps, 'onChange' | 'value' | 'defaultValue'> & {
  onChange?: (value: string) => void
  value?: string
  defaultValue?: string
  accurate?: any
}

export function AccurateBranchPicker({ value, onChange, accurate, defaultValue, ...otherProps }: AccurateBranchPickerProps) {
  const [computedValue, triggerChange] = useCustomComponent({ value, defaultValue, onChange })

  const handleChange = (e:any) => {
    triggerChange(e.target.value)
  }
  return (
    <Container>
      <div className='s-1'>
        <Input {...otherProps} value={computedValue} onChange={handleChange} />
      </div>
      <div className='s-2'>
        <SelectBranchButton accurate={accurate} onSelected={v => triggerChange(v)}>
          <Button>Pilih</Button>
        </SelectBranchButton>
      </div>
    </Container>
  )
}
