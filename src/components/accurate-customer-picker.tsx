import { Button, Input, InputProps, List, Modal, Pagination, Space } from 'antd'
import { ReactElement, cloneElement, useState } from 'react'
import { useQuery } from 'react-query'
import { styled } from 'styled-components'
import { useCustomComponent } from '../hooks/use-custom-component'
import { AccurateService } from '../services/accurate'

type SelectCustomerButtonProps = {
  children: ReactElement
  accurate: any
  onSelected: (v: string) => void
}
function SelectCustomerButton({ accurate, children, onSelected }: SelectCustomerButtonProps) {
  const [open, setOpen] = useState(false)
  const [q, setQ] = useState('')
  const [page, setPage] = useState(1)
  const { data, isLoading } = useQuery(`accurate.customers[${q}${page}]`, () => AccurateService.findManyCustomers(accurate.id, q, page), { enabled: open && !!accurate })
  const databases: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleSelect = (c: any) => {
    onSelected(c.customerNo)
    handleClose()
  }
  return (
    <>
      {cloneElement(children, { onClick: handleClick })}
      <Modal
        title="Pilih Customer"
        destroyOnClose
        open={open}
        centered
        footer={null}
        onCancel={handleClose}
      >
        <Input.Search
          onSearch={q => {
            setQ(q)
            setPage(1)
          }}
        />
        <List
          dataSource={databases}
          loading={isLoading}
          renderItem={c => (
            <List.Item extra={<Button disabled={!accurate} onClick={() => handleSelect(c)}>Pilih</Button>}>
              <List.Item.Meta
                title={<Space><div>{c.name}</div><div>({c.customerNo})</div></Space>}
                description={c.email || <i>Tidak ada email</i>}
              />
            </List.Item>
          )}
          footer={
            <Pagination
              pageSize={1}
              total={meta?.pageCount || 1}
              current={meta?.page || 1}
              onChange={p => setPage(p)}
            />
          }
        />
      </Modal>
    </>
  )
}

const Container = styled.div`
display: flex;
gap: 4px;
.s-1 {
  flex: 1;
}
.s-2 {
  flex: 0;
}
`

type AccurateCustomerPickerProps = Omit<InputProps, 'onChange' | 'value' | 'defaultValue'> & {
  onChange?: (value: string) => void
  value?: string
  defaultValue?: string
  accurate?: any
}

export function AccurateCustomerPicker({ value, onChange, accurate, defaultValue, ...otherProps }: AccurateCustomerPickerProps) {
  const [computedValue, triggerChange] = useCustomComponent({ value, defaultValue, onChange })

  const handleChange = (e:any) => {
    triggerChange(e.target.value)
  }
  return (
    <Container>
      <div className='s-1'>
        <Input {...otherProps} value={computedValue} onChange={handleChange} />
      </div>
      <div className='s-2'>
        <SelectCustomerButton accurate={accurate} onSelected={v => triggerChange(v)}>
          <Button>Pilih</Button>
        </SelectCustomerButton>
      </div>
    </Container>
  )
}
