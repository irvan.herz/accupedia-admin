import { Button, Input, InputProps, List, Modal } from 'antd'
import { ReactElement, cloneElement, useState } from 'react'
import { useQuery } from 'react-query'
import { styled } from 'styled-components'
import { useCustomComponent } from '../hooks/use-custom-component'
import { AccurateService } from '../services/accurate'

type SelectDbButtonProps = {
  children: ReactElement
  accurate: any
  onSelected: (v: string) => void
}
function SelectDbButton({ accurate, children, onSelected }: SelectDbButtonProps) {
  const [open, setOpen] = useState(false)
  const { data, isLoading } = useQuery('accurate.databases[]', () => AccurateService.findManyDatabases(accurate.id), { enabled: open && !!accurate })
  const databases: any[] = data?.data || []

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleSelect = (db: any) => {
    onSelected(db.id)
    handleClose()
  }
  return (
    <>
      {cloneElement(children, { onClick: handleClick })}
      <Modal
        title="Pilih DB"
        destroyOnClose
        open={open}
        centered
        footer={null}
        onCancel={handleClose}
      >
        <List
          dataSource={databases}
          loading={isLoading}
          renderItem={db => (
            <List.Item extra={<Button disabled={!accurate} onClick={() => handleSelect(db)}>Pilih</Button>}>
              <List.Item.Meta
                title={db.alias}
                description={db.expired ? 'Expired' : 'Available'}
              />
            </List.Item>
          )}
        />
      </Modal>
    </>
  )
}

const Container = styled.div`
display: flex;
gap: 4px;
.s-1 {
  flex: 1;
}
.s-2 {
  flex: 0;
}
`

type AccurateDbPickerProps = Omit<InputProps, 'onChange' | 'value' | 'defaultValue'> & {
  onChange?: (value: string) => void
  value?: string
  defaultValue?: string
  accurate?: any
}

export function AccurateDbPicker({ value, onChange, accurate, defaultValue, ...otherProps }: AccurateDbPickerProps) {
  const [computedValue, triggerChange] = useCustomComponent({ value, defaultValue, onChange })

  const handleChange = (e:any) => {
    triggerChange(e.target.value)
  }
  return (
    <Container>
      <div className='s-1'>
        <Input {...otherProps} value={computedValue} onChange={handleChange} />
      </div>
      <div className='s-2'>
        <SelectDbButton accurate={accurate} onSelected={v => triggerChange(v)}>
          <Button>Pilih</Button>
        </SelectDbButton>
      </div>
    </Container>
  )
}
