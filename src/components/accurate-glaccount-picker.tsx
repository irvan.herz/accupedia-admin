import { Button, Input, InputProps, List, Modal, Pagination, Space } from 'antd'
import { ReactElement, cloneElement, useState } from 'react'
import { useQuery } from 'react-query'
import { styled } from 'styled-components'
import { useCustomComponent } from '../hooks/use-custom-component'
import { AccurateService } from '../services/accurate'

type SelectGlAccountButtonProps = {
  children: ReactElement
  accurate: any
  onSelected: (v: string) => void
}
function SelectGlAccountButton({ accurate, children, onSelected }: SelectGlAccountButtonProps) {
  const [open, setOpen] = useState(false)
  const [q, setQ] = useState('')
  const [page, setPage] = useState(1)
  const { data, isLoading } = useQuery(`accurate.glaccounts[${q}${page}]`, () => AccurateService.findManyGlAccounts(accurate.id, q, page), { enabled: open && !!accurate })
  const databases: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleSelect = (ga: any) => {
    onSelected(ga.no)
    handleClose()
  }
  return (
    <>
      {cloneElement(children, { onClick: handleClick })}
      <Modal
        title="Pilih General Ledger Account"
        destroyOnClose
        open={open}
        centered
        footer={null}
        onCancel={handleClose}
      >
        <Input.Search
          onSearch={q => {
            setQ(q)
            setPage(1)
          }}
        />
        <List
          dataSource={databases}
          loading={isLoading}
          renderItem={ga => (
            <List.Item extra={<Button disabled={!accurate} onClick={() => handleSelect(ga)}>Pilih</Button>}>
              <List.Item.Meta
                title={<Space><div>{ga.name}</div><div>({ga.accountType})</div></Space>}
                description={ga.no}
              />
            </List.Item>
          )}
          footer={
            <Pagination
              pageSize={1}
              total={meta?.pageCount || 1}
              current={meta?.page || 1}
              onChange={p => setPage(p)}
            />
          }
        />
      </Modal>
    </>
  )
}

const Container = styled.div`
display: flex;
gap: 4px;
.s-1 {
  flex: 1;
}
.s-2 {
  flex: 0;
}
`

type AccurateGlAccountPickerProps = Omit<InputProps, 'onChange' | 'value' | 'defaultValue'> & {
  onChange?: (value: string) => void
  value?: string
  defaultValue?: string
  accurate?: any
}

export function AccurateGlAccountPicker({ value, onChange, accurate, defaultValue, ...otherProps }: AccurateGlAccountPickerProps) {
  const [computedValue, triggerChange] = useCustomComponent({ value, defaultValue, onChange })

  const handleChange = (e:any) => {
    triggerChange(e.target.value)
  }
  return (
    <Container>
      <div className='s-1'>
        <Input {...otherProps} value={computedValue} onChange={handleChange} />
      </div>
      <div className='s-2'>
        <SelectGlAccountButton accurate={accurate} onSelected={v => triggerChange(v)}>
          <Button>Pilih</Button>
        </SelectGlAccountButton>
      </div>
    </Container>
  )
}
