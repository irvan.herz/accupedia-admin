import { Select, SelectProps } from 'antd'
import { debounce } from 'lodash'
import { useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useCustomComponent } from '../hooks/use-custom-component'
import { AccurateService } from '../services/accurate'

export type AccurateIdInputProps = SelectProps
export function AccurateIdInput ({ value, defaultValue, onChange, ...otherProps }:AccurateIdInputProps) {
  const [computedValue, triggerValueChange] = useCustomComponent({ value, defaultValue, onChange: onChange as any })
  const [search, setSearch] = useState('')
  const accurateQuery = useQuery(`accurate[${computedValue}]`, () => AccurateService.findById(computedValue), { enabled: !!computedValue })
  const accuratesQuery = useQuery(`accurate[search:${search}]`, () => AccurateService.findMany({ search }))
  const accurates = accuratesQuery?.data?.data || []
  const accurate = accurateQuery?.data?.data

  const setSearchDebounced = debounce(e => setSearch(e), 1000)

  const options = useMemo(() => {
    const accuratesById: any = {}
    if (accurate) {
      accuratesById[accurate.id] = accurate
    }
    accurates.forEach((u: any) => {
      accuratesById[u.id] = u
    })
    return Object.keys(accuratesById).map(id => {
      return {
        key: accuratesById[id].id,
        value: accuratesById[id].id,
        label: accuratesById[id].email || `Accurate ${accuratesById[id].id}`
      }
    })
  }, [accurate, accurates])

  return (
    <Select
      showSearch
      allowClear
      value={+computedValue || undefined}
      placeholder="Select accurate..."
      defaultActiveFirstOption={false}
      filterOption={false}
      onSearch={setSearchDebounced}
      onChange={triggerValueChange}
      notFoundContent={null}
      options={options}
      style={{ width: '100%' }}
      {...otherProps}
    />
  )
}
