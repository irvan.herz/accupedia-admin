import { Button, Input, InputProps, List, Modal, Pagination, Space } from 'antd'
import { ReactElement, cloneElement, useState } from 'react'
import { useQuery } from 'react-query'
import { styled } from 'styled-components'
import { useCustomComponent } from '../hooks/use-custom-component'
import { AccurateService } from '../services/accurate'

type SelectWarehouseButtonProps = {
  children: ReactElement
  accurate: any
  onSelected: (v: any) => void
}
function SelectWarehouseButton({ accurate, children, onSelected }: SelectWarehouseButtonProps) {
  const [open, setOpen] = useState(false)
  const [q, setQ] = useState('')
  const [page, setPage] = useState(1)
  const { data, isLoading } = useQuery(`accurate.warehouses[${q}${page}]`, () => AccurateService.findManyWarehouses(accurate.id, q, page), { enabled: open && !!accurate })
  const warehouses: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleSelect = (wr: any) => {
    onSelected(wr)
    handleClose()
  }
  return (
    <>
      {cloneElement(children, { onClick: handleClick })}
      <Modal
        title="Pilih Gudang"
        destroyOnClose
        open={open}
        centered
        footer={null}
        onCancel={handleClose}
      >
        <Input.Search
          onSearch={q => {
            setQ(q)
            setPage(1)
          }}
        />
        <List
          dataSource={warehouses}
          loading={isLoading}
          renderItem={wr => (
            <List.Item extra={<Button disabled={!accurate} onClick={() => handleSelect(wr)}>Pilih</Button>}>
              <List.Item.Meta
                title={<Space><div>{wr.name}</div><div>({wr.id})</div></Space>}
                description={wr.description || <i> Tidak ada deskripsi</i>}
              />
            </List.Item>
          )}
          footer={
            <Pagination
              pageSize={1}
              total={meta?.pageCount || 1}
              current={meta?.page || 1}
              onChange={p => setPage(p)}
            />
          }
        />
      </Modal>
    </>
  )
}

const Container = styled.div`
display: flex;
gap: 4px;
.s-1 {
  flex: 1;
}
.s-2 {
  flex: 0;
}
`

type AccurateWarehousePickerProps = Omit<InputProps, 'onChange' | 'value' | 'defaultValue'> & {
  onChange?: (value: string) => void
  onWarehouseSelected?: (v:any) => void
  value?: string
  defaultValue?: string
  accurate?: any
}

export function AccurateWarehousePicker({ value, onChange, onWarehouseSelected, accurate, defaultValue, ...otherProps }: AccurateWarehousePickerProps) {
  const [computedValue, triggerChange] = useCustomComponent({ value, defaultValue, onChange })

  const handleChange = (e:any) => {
    triggerChange(e.target.value)
  }
  return (
    <Container>
      <div className='s-1'>
        <Input {...otherProps} value={computedValue} onChange={handleChange} />
      </div>
      <div className='s-2'>
        <SelectWarehouseButton
          accurate={accurate}
          onSelected={v => {
            triggerChange(v.id)
            onWarehouseSelected?.(v)
          }
      }>
          <Button>Pilih</Button>
        </SelectWarehouseButton>
      </div>
    </Container>
  )
}
