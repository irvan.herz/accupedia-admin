import { Label } from '@windmill/react-ui'
import classNames from 'classnames'
import { ReactElement, cloneElement } from 'react'
import { Controller, ControllerProps, useFormContext } from 'react-hook-form'

type FormItemProps = {
  children: ReactElement
  name: string
  label: string
  rules?: ControllerProps['rules']
  labelClassName?: string
  wrapperClassName?: string
}

export function FormItem({ name, children, label, rules, labelClassName, wrapperClassName }: FormItemProps) {
  const { control, formState } = useFormContext()
  const errorMessage = formState.errors[name] ? formState.errors[name]?.message?.toString() || 'Tidak valid' : null

  return (
    <div className="grid grid-cols-12 gap-3 sm:gap-6 mb-6">
      <Label className={classNames('col-span-12 sm:col-span-4', labelClassName)}>{label}</Label>
      <div className={classNames('col-span-12 sm:col-span-8', wrapperClassName)}>
        <Controller
          control={control}
          rules={rules}
          name={name}
          render={({ field: { value, onChange, onBlur } }) => (
            cloneElement(children, { value, onChange, onBlur })
          )}
        />
        {errorMessage && <span className='text-red-400 text-sm mt-2'>{errorMessage}</span>}
      </div>
    </div>
  )
}
