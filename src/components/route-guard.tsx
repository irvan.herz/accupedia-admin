import { AuthContext } from 'contexts/auth-context'
import { ReactElement, ReactNode, useContext } from 'react'
import { Navigate } from 'react-router-dom'

type RouteGuardProps = {
  children: ReactElement
  require: 'authenticated' | 'unauthenticated' | undefined
  onRejected?: () => ReactNode | null | undefined
}

export function RouteGuard ({ children, require, onRejected }: RouteGuardProps) {
  const auth = useContext(AuthContext)

  const REDIR = {
    authenticated: '/auth/signin',
    unauthenticated: '/'
  }

  if (require && require !== auth.status) {
    if (onRejected) {
      const red = onRejected()
      return (<>{red}</>)
    } else {
      return <Navigate replace to={REDIR[require]} />
    }
  } else {
    return children
  }
}
