import { ReactNode, createContext, useState } from 'react'

export type AdminLayoutContextType = {
  sidebarOpen: boolean
  setSidebarOpen: (o:boolean) => void
  activeMenuKey: string
  setActiveMenuKey: (o:string) => void
  expandedMenuKey: string
  setExpandedMenuKey: (o:string) => void
}

export const AdminLayoutContext = createContext<AdminLayoutContextType>({
  sidebarOpen: true,
  setSidebarOpen: () => {},
  activeMenuKey: 'dashboard',
  setActiveMenuKey: () => {},
  expandedMenuKey: '',
  setExpandedMenuKey: () => {}
})

type AdminLayoutContextProviderProps = {
  children: ReactNode
}
export function AdminLayoutContextProvider({ children }:AdminLayoutContextProviderProps) {
  const [sidebarOpen, setSidebarOpen] = useState<boolean>(true)
  const [activeMenuKey, setActiveMenuKey] = useState('dashboard')
  const [expandedMenuKey, setExpandedMenuKey] = useState('')

  return (
    <AdminLayoutContext.Provider value={{ sidebarOpen, setSidebarOpen, activeMenuKey, setActiveMenuKey, expandedMenuKey, setExpandedMenuKey }}>
      {children}
    </AdminLayoutContext.Provider>
  )
}
