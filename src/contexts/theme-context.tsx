import { ReactNode, createContext, useState } from 'react'

export type THEME = 'light' | 'dark'

export type ThemeContextType = {
  theme: THEME
  setTheme: (t:THEME) => void
}

export const ThemeContext = createContext<ThemeContextType>({
  theme: 'light',
  setTheme: () => {}
})

type ThemeContextProviderProps = {
  children: ReactNode
}
export function ThemeContextProvider({ children }:ThemeContextProviderProps) {
  const [theme, setTheme] = useState<THEME>('light')
  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}
