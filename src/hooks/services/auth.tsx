import { useMutation } from 'react-query'
import { AuthService } from 'services/auth-service'

type UseSigninArgs = {
  data: any
}
export function useSignin() {
  return useMutation<any, any, UseSigninArgs>(
    payload => AuthService.signin(payload.data)
  )
}
