import { useMutation, useQuery } from 'react-query'
import { OrdersService } from 'services/orders-service'

export function useOrders(filter: any) {
  return useQuery(
    ['orders', filter],
    () => OrdersService.findMany(filter)
  )
}

type UseCreateOrderArgs = {
  data: any
}
export function useCreateOrder() {
  return useMutation<any, any, UseCreateOrderArgs>(
    payload => OrdersService.create(payload.data)
  )
}

type UseDeleteOrderArgs = {
  id: number
}

export function useDeleteOrder() {
  return useMutation<any, any, UseDeleteOrderArgs>(
    payload => OrdersService.deleteById(payload.id)
  )
}

type UseUpdateOrderArgs = {
  id: number
  data: any
}

export function useUpdateOrder() {
  return useMutation<any, any, UseUpdateOrderArgs>(
    payload => OrdersService.updateById(payload.id, payload.data)
  )
}
