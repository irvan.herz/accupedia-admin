import { useMutation, useQuery } from 'react-query'
import { ProductsService } from 'services/products-service'

export function useProducts(filter: any) {
  return useQuery(
    ['products', filter],
    () => ProductsService.findMany(filter)
  )
}

type UseCreateProductArgs = {
  data: any
}
export function useCreateProduct() {
  return useMutation<any, any, UseCreateProductArgs>(
    payload => ProductsService.create(payload.data)
  )
}

type UseDeleteProductArgs = {
  id: number
}

export function useDeleteProduct() {
  return useMutation<any, any, UseDeleteProductArgs>(
    payload => ProductsService.deleteById(payload.id)
  )
}

type UseUpdateProductArgs = {
  id: number
  data: any
}

export function useUpdateProduct() {
  return useMutation<any, any, UseUpdateProductArgs>(
    payload => ProductsService.updateById(payload.id, payload.data)
  )
}

export function useProductCategories(filter: any) {
  return useQuery(
    ['products.categories', filter],
    () => ProductsService.Categories.findMany(filter)
  )
}

type UseCreateProductCategoryArgs = {
  data: any
}
export function useCreateProductCategory() {
  return useMutation<any, any, UseCreateProductCategoryArgs>(
    payload => ProductsService.Categories.create(payload.data)
  )
}

type UseDeleteProductCategoryArgs = {
  id: number
}

export function useDeleteProductCategory() {
  return useMutation<any, any, UseDeleteProductCategoryArgs>(
    payload => ProductsService.Categories.deleteById(payload.id)
  )
}

type UseUpdateProductCategoryArgs = {
  id: number
  data: any
}

export function useUpdateProductCategory() {
  return useMutation<any, any, UseUpdateProductCategoryArgs>(
    payload => ProductsService.Categories.updateById(payload.id, payload.data)
  )
}
