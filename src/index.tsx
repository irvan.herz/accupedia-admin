import { Windmill } from '@windmill/react-ui'
import 'assets/css/custom.css'
import 'assets/css/rc-picker.scss'
import { AxiosInterceptor } from 'components/axios-interceptor'
import { AdminLayoutContextProvider } from 'contexts/admin-layout-context'
import { AuthContextProvider } from 'contexts/auth-context'
import { CurrentUserContextProvider } from 'contexts/current-user-context'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { DefaultWindmillTheme } from 'theme'
import App from './App'
import './index.css'
import reportWebVitals from './reportWebVitals'
dayjs.extend(relativeTime)
const queryClient = new QueryClient({ defaultOptions: { queries: { refetchOnWindowFocus: false } } })

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
)

root.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <AuthContextProvider>
        <AxiosInterceptor>
          <CurrentUserContextProvider>
            <Windmill usePreferences theme={DefaultWindmillTheme}>
              <AdminLayoutContextProvider>
                <App />
                <div id='modals-container' />
                <div id='alerts-container' />
                <ToastContainer
                  position="top-center"
                  autoClose={3000}
                  hideProgressBar={false}
                  newestOnTop={false}
                  closeOnClick
                  rtl={false}
                  pauseOnFocusLoss
                  draggable
                  pauseOnHover/>
              </AdminLayoutContextProvider>
            </Windmill>
          </CurrentUserContextProvider>
        </AxiosInterceptor>
      </AuthContextProvider>
    </QueryClientProvider>
  </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
