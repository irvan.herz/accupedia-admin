import classNames from 'classnames'
import { ReactNode } from 'react'

type AdminBodyScaffoldProps = {
  children: ReactNode
  title: ReactNode
  className?: string
  contentContainerClassName?: string
}
export function AdminBodyScaffold({ children, title, className, contentContainerClassName }:AdminBodyScaffoldProps) {
  return (
    <div className={classNames('p-6 flex flex-col gap-6', className)}>
      {!!title && (
        <div className="font-bold text-xl text-black dark:text-white">{title}</div>
      )}
      <div className={contentContainerClassName}>{children}</div>
    </div>
  )
}
