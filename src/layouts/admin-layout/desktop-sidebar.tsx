import SidebarContent from './sidebar-content'

const DesktopSidebar = () => {
  return (
    <aside className="flex-shrink-0 shadow-sm w-64 overflow-y-auto bg-white dark:bg-gray-800">
      <SidebarContent />
    </aside>
  )
}

export default DesktopSidebar
