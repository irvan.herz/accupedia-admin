import classNames from 'classnames'
import { RouteGuard } from 'components/route-guard'
import { AdminLayoutContext } from 'contexts/admin-layout-context'
import { useWindowDimensions } from 'hooks/use-window-dimensions'
import { ReactNode, Suspense, useContext, useEffect, useMemo } from 'react'
import DesktopSidebar from './desktop-sidebar'
import Header from './header'
import MobileSidebar from './mobile-sidebar'

function Suspenser() {
  return (
    <div className="w-full h-screen p-6 text-lg font-medium text-gray-600 dark:text-gray-400 dark:bg-gray-900">
      Loading...
    </div>
  )
}

type AdminLayoutProps = {
  children: ReactNode,
  activeMenuKey?: string
  expandedMenuKey?: string
}

export function AdminLayout({ children, activeMenuKey, expandedMenuKey }: AdminLayoutProps) {
  const { sidebarOpen, setActiveMenuKey, setExpandedMenuKey } = useContext(AdminLayoutContext)
  const window = useWindowDimensions()

  useEffect(() => {
    setActiveMenuKey(activeMenuKey || 'dashboard')
  }, [activeMenuKey])

  useEffect(() => {
    setExpandedMenuKey(expandedMenuKey || '')
  }, [expandedMenuKey])

  const isMobile = useMemo(() => {
    return window.width < 720
  }, [window.width])

  return (
    <RouteGuard require='authenticated'>
      <div
        className={classNames('flex h-screen bg-gray-50 dark:bg-gray-900')}>
        {sidebarOpen && (isMobile ? <MobileSidebar /> : <DesktopSidebar />)}
        <div className="flex flex-col flex-1 w-full">
          <Header />
          <main className="h-full overflow-y-auto">
            <div className="container grid mx-auto">
              <Suspense fallback={<Suspenser />}>
                {children}
              </Suspense>
            </div>
          </main>
        </div>
      </div>
    </RouteGuard>
  )
}
