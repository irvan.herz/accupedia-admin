import { Backdrop, Transition } from '@windmill/react-ui'
import { AdminLayoutContext } from 'contexts/admin-layout-context'
import { useContext } from 'react'
import SidebarContent from './sidebar-content'

function MobileSidebar() {
  const { sidebarOpen, setSidebarOpen } = useContext(AdminLayoutContext)

  return (
    <Transition show={sidebarOpen}>
      <>
        <Transition
          enter="transition ease-in-out duration-150"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="transition ease-in-out duration-150"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Backdrop onClick={() => setSidebarOpen(!sidebarOpen)} />
        </Transition>

        <Transition
          enter="transition ease-in-out duration-150"
          enterFrom="opacity-0 transform -translate-x-20"
          enterTo="opacity-100"
          leave="transition ease-in-out duration-150"
          leaveFrom="opacity-100"
          leaveTo="opacity-0 transform -translate-x-20"
        >
          <aside className="fixed inset-y-0 flex-shrink-0 w-64 mt-16 overflow-y-auto bg-white dark:bg-gray-800 lg:hidden">
            <SidebarContent />
          </aside>
        </Transition>
      </>
    </Transition>
  )
}

export default MobileSidebar
