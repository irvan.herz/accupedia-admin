import { Button, WindmillContext } from '@windmill/react-ui'
import { useContext } from 'react'
import { IoChevronDownOutline, IoChevronForwardOutline, IoLogOutOutline, IoRemoveSharp } from 'react-icons/io5'
import { NavLink } from 'react-router-dom'

import classNames from 'classnames'
import { AdminLayoutContext } from 'contexts/admin-layout-context'
import { sidebarRoutes } from 'routes/sidebar'

type SidebarExtendedMenuItemProps = {
  route: any
}

function SidebarExtendedMenuItem({ route }: SidebarExtendedMenuItemProps) {
  const { activeMenuKey, expandedMenuKey, setExpandedMenuKey } = useContext(AdminLayoutContext)
  const open = expandedMenuKey === route.key
  const handleSubMenu = () => setExpandedMenuKey(expandedMenuKey ? '' : route.key)

  return (
    <>
      <li className="relative px-6 py-3" key={route.name}>
        <button
          className="inline-flex items-center justify-between focus:outline-none w-full text-sm font-semibold transition-colors duration-150 hover:text-green-600 dark:hover:text-gray-200"
          onClick={handleSubMenu}
          aria-haspopup="true"
        >
          <span
            className={classNames('absolute inset-y-0 left-0 w-1 bg-green-600 rounded-tr-lg rounded-br-lg', activeMenuKey === route.key ? 'block' : 'hidden')}
            aria-hidden="true" />
          <span className="inline-flex items-center">
            <route.icon className="w-5 h-5" aria-hidden="true" />
            <span className="ml-4 mt-1">{route.name}</span>
            <span className="pl-4 mt-1">
              {open ? <IoChevronDownOutline /> : <IoChevronForwardOutline />}
            </span>
          </span>
          {/* <DropdownIcon className="w-4 h-4" aria-hidden="true" /> */}
        </button>
        {open && (
          <ul
            className="p-2  overflow-hidden text-sm font-medium text-gray-500 rounded-md dark:text-gray-400 dark:bg-gray-900"
            aria-label="submenu"
          >
            {route.routes.map((child: any, i: number) => (
              <li key={i + 1}>
                <NavLink
                  to={child.path}
                  className="flex items-center font-serif py-1 text-sm text-gray-600 hover:text-green-600 cursor-pointer"
                >
                  <span className="text-xs text-gray-500 pr-1">
                    <IoRemoveSharp />
                  </span>
                  <span className="text-gray-500 hover:text-green-600 dark:hover:text-gray-200">
                    {child.name}
                  </span>
                </NavLink>
              </li>
            ))}
          </ul>
        )}
      </li>
    </>
  )
}

type SidebarMenuItemProps = {
  route: any
}

function SidebarMenuItem({ route }: SidebarMenuItemProps) {
  const { activeMenuKey } = useContext(AdminLayoutContext)
  if (route.routes) {
    return <SidebarExtendedMenuItem route={route} key={route.name} />
  } else {
    return (
      <li className="relative" key={route.name}>
        <NavLink
          to={route.path}
          target={`${route?.outside ? '_blank' : '_self'}`}
          className="px-6 py-4 inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-green-700 dark:hover:text-gray-200">
          <span
            className={classNames('absolute inset-y-0 left-0 w-1 bg-green-500 rounded-tr-lg rounded-br-lg', activeMenuKey === route.key ? 'block' : 'hidden')}
            aria-hidden="true" />
          <route.icon className="w-5 h-5" aria-hidden="true" />
          <span className="ml-4">{route.name}</span>
        </NavLink>
      </li>
    )
  }
}

type SidebarMenuProps = {
  routes: any[]
}

function SidebarMenu({ routes }: SidebarMenuProps) {
  return (
    <>
      {routes.map(route => <SidebarMenuItem key={route.key} route={route} />)}
    </>
  )
}

const SidebarContent = () => {
  const { mode } = useContext(WindmillContext)

  const handleLogOut = () => {
  }

  return (
    <div className="py-4 text-gray-500 dark:text-gray-400">
      <a className=" text-gray-900 dark:text-gray-200" href="/dashboard">
        {mode === 'dark'
          ? (
            <img src={`${process.env.PUBLIC_URL}/assets/images/logo-text-light.svg`} alt="dashtar" width="200" className="pl-6" />
            )
          : (
            <img src={`${process.env.PUBLIC_URL}/assets/images/logo-text-dark.svg`} alt="dashtar" width="200" className="pl-6" />
            )}
      </a>
      <ul className="mt-8">
        <SidebarMenu routes={sidebarRoutes} />
      </ul>
      <span className="lg:fixed bottom-0 px-6 py-6 w-64 mx-auto relative mt-3 block">
        <Button onClick={handleLogOut} size="large" className="w-full">
          <span className="flex items-center">
            <IoLogOutOutline className="mr-3 text-lg" />
            <span className="text-sm">Log Out</span>
          </span>
        </Button>
      </span>
    </div>
  )
}

export default SidebarContent
