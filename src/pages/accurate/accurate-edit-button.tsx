import { Form, FormProps, Modal, message } from 'antd'
import { ReactElement, cloneElement, useEffect, useState } from 'react'
import { useMutation } from 'react-query'
import { AccurateService } from '../../services/accurate'
import { AccurateForm } from './accurate-form'

type AccurateEditButtonProps = {
  accurate: any
  children: ReactElement
  afterUpdated?: any
}

export function AccurateEditButton({ accurate, children, afterUpdated }: AccurateEditButtonProps) {
  const [open, setOpen] = useState(false)
  const [form] = Form.useForm()
  const updater = useMutation(x => AccurateService.updateById(accurate.id, x))

  useEffect(() => {
    if (open) form.setFieldsValue(accurate)
  }, [open, accurate])

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleFinish: FormProps['onFinish'] = async (values) => {
    try {
      await updater.mutateAsync(values)
      afterUpdated?.()
      handleClose()
    } catch (err:any) {
      message.error(err?.message || 'Terjadi kesalahan')
    }
  }

  const handleFinishFailed: FormProps['onFinishFailed'] = (errorInfo) => {
    message.error('Cek ulang form')
  }

  return (
    <>
      {cloneElement(children, { onClick: handleClick })}
      <Modal
        title="Edit Accurate"
        open={open}
        centered
        confirmLoading={updater.isLoading}
        onOk={form.submit}
        onCancel={handleClose}
      >
        <Form form={form} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} onFinish={handleFinish} onFinishFailed={handleFinishFailed}>
          <AccurateForm accurate={accurate}/>
        </Form>
      </Modal>
    </>
  )
}
