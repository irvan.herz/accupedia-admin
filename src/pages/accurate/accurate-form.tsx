import { Form, Input, Select } from 'antd'
import { AccurateCustomerPicker } from '../../components/accurate-customer-picker'
import { AccurateDbPicker } from '../../components/accurate-db-picker'
import { AccurateGlAccountPicker } from '../../components/accurate-glaccount-picker'
import { AccurateWarehousePicker } from '../../components/accurate-warehouse-picker'

type AccurateFormProps = {
  accurate?: any
}

export function AccurateForm({ accurate }: AccurateFormProps) {
  const form = Form.useFormInstance()

  const handleWarehouseSelected = (w: any) => {
    form.setFieldValue('defaultWarehouseName', w.name)
  }

  return (
    <Form.Provider>
      <Form.Item
        label="Email"
        name="email"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Database ID"
        name="databaseId"
        help="Pilih database accurate"
      >
        <AccurateDbPicker accurate={accurate} />
      </Form.Item>
      <Form.Item
        label="Discount Account No"
        name="defaultDiscountAccountNo"
        help="Pilih akun perkiraan untuk biaya admin tokopedia"
      >
        <AccurateGlAccountPicker accurate={accurate} />
      </Form.Item>
      <Form.Item
        label="Default Customer No Tokopedia"
        name="defaultCustomerNo"
        help="Pilih kode pelanggan sales invoice"
      >
        <AccurateCustomerPicker accurate={accurate} />
      </Form.Item>
      <Form.Item
        label="Default Disccount Account No"
        name="defaultDiscountAccountNo"
        help="Pilih akun perkiraan untuk biaya admin tokopedia"
      >
        <AccurateGlAccountPicker accurate={accurate} />
      </Form.Item>
      <Form.Item
        label="Default Bank No"
        name="defaultBankNo"
        help="Pilih akun perkiraan kas bank untuk sales receipt"
      >
        <AccurateGlAccountPicker accurate={accurate} />
      </Form.Item>
      <Form.Item
        label="Default Warehouse ID"
        help="Kode gudang barang accurate untuk marketplace. Jika ada perubahan stok di gudang terpilih, stok di Tokopedia akan disinkronkan"
        name="defaultWarehouseId"
      >
        <AccurateWarehousePicker accurate={accurate} onWarehouseSelected={handleWarehouseSelected}/>
      </Form.Item>
      <Form.Item
        label="Default Warehouse Name"
        help="Nama gudang barang accurate untuk marketplace. Nama harus sesuai dengan warehouse ID"
        name="defaultWarehouseName"
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Host"
        name="host"
        help="Tidak perlu diisi manual. Kecuali diperlukan. Server accurate ini seharusnya terisi otomatis saat link akun"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Token"
        name="accessToken"
        help="Tidak perlu diisi manual. Kecuali diperlukan. Kode akses ini seharusnya terisi otomatis saat link akun"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Refresh Token"
        name="refreshToken"
        help="Tidak perlu diisi manual. Kecuali diperlukan. Kode akses ini seharusnya terisi otomatis saat link akun"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Sync Stock"
        name="syncStockStatus"
        help="Jika aktif, stok tokopedia akan disinkronkan dari stok barang accurate. Barang yang disinkron menyesuakan dengan data default warehouse id"
      >
        <Select
          options={[
            { value: 'disabled', label: 'Tidak Aktif' },
            { value: 'enabled', label: 'Aktif' }
          ]}
        />
      </Form.Item>
    </Form.Provider>
  )
}
