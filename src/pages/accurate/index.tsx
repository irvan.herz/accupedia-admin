import { Button, Col, Input, List, Pagination, Row, Select, Space, Typography } from 'antd'
import dayjs from 'dayjs'
import { API_BASE_URL } from 'libs/variables'
import { useQuery } from 'react-query'
import { Link } from 'react-router-dom'
import { StdLayout } from '../../components/standard-layout'
import { FilterConfig, useQueryFilters } from '../../hooks/use-query-filter'
import { AccurateService } from '../../services/accurate'
import { AccurateEditButton } from './accurate-edit-button'

const SORT_OPTIONS = [{ label: 'Newest', value: 'newest' }, { label: 'Oldest', value: 'oldest' }]

const FILT: Record<string, FilterConfig> = {
  search: {
    match: /.*/,
    default: ''
  },
  page: {
    match: /[0-9]+/,
    default: '1'
  },
  sort: {
    match: /(newest|oldest)/,
    default: 'newest',
    translate: value => {
      const MAP = {
        newest: { sortBy: 'createdAt', sortOrder: 'desc' },
        oldest: { sortBy: 'createdAt', sortOrder: 'asc' }
      }
      const key = value as keyof typeof MAP
      return MAP[key] || MAP.newest
    }
  }
}

type AccurateItemProps = {
  accurate: any,
  afterUpdated?: any
}

function AccurateItem({ accurate, afterUpdated }: AccurateItemProps) {
  return (
    <List.Item extra={dayjs(accurate.createdAt).fromNow()}>
      <List.Item.Meta
        title={accurate.email}
        description={
          <Space direction='vertical' size='small'>
            <div>Host: {accurate.host ? `${accurate.host}` : <i>Belum set</i>}</div>
            <div>Database ID: {accurate.databaseId ? `${accurate.databaseId}` : <i>Belum set</i>}</div>
            <div><AccurateEditButton accurate={accurate} afterUpdated={afterUpdated}><Button size='small'>Edit</Button></AccurateEditButton></div>
          </Space>
        }
      />
    </List.Item>
  )
}

export function AccuratePage() {
  const [filters, apiFilter, refilter] = useQueryFilters(FILT)
  const { data, refetch, isLoading } = useQuery(['accurate[]', apiFilter], () => AccurateService.findMany(apiFilter))
  const accurates: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleRefresh = () => refetch()

  return (
    <StdLayout
      menuProps={{ selectedKeys: ['accurate'] }}
      applet={
        <Row gutter={8}>
          <Col span={12}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Search</Typography.Text>
              <Input.Search allowClear defaultValue={filters.search} onSearch={q => refilter({ search: q, page: 1 })} placeholder='Cari akun accurate...' />
            </Space>
          </Col>
          <Col span={8}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Sort</Typography.Text>
              <Select defaultValue={filters.sort} onChange={v => refilter({ sort: v })} options={SORT_OPTIONS} style={{ width: '100%' }} placeholder="Sort..." />
            </Space>
          </Col>
          <Col span={4}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Action</Typography.Text>
              <Button onClick={handleRefresh} loading={isLoading}>Refresh</Button>
            </Space>
          </Col>
        </Row>
      }
      bottomApplet={
        <Link to={`${API_BASE_URL}/accurate/authorize`}>
          <Button type='primary'>Link Akun Baru</Button>
        </Link>
      }
    >
      <List
        dataSource={accurates}
        renderItem={accurate => (
          <AccurateItem accurate={accurate} afterUpdated={refetch}/>
        )}
        footer={
          <Pagination
            pageSize={1}
            total={meta?.numPages}
            current={meta?.page || 0}
            onChange={p => refilter({ page: p })}
          />
        }
      />
    </StdLayout>
  )
}
