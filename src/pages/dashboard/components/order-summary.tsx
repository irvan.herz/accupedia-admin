import { Card, CardBody, WindmillContext } from '@windmill/react-ui'
import classNames from 'classnames'
import { ReactNode, useContext } from 'react'
import { ImStack } from 'react-icons/im'
import Skeleton from 'react-loading-skeleton'

type SummaryCardProps = {
  icon: ReactNode
  title: string
  value: string
  isLoading?: boolean
  bodyClassName?: string
}

function SummaryCard({ icon, title, value, bodyClassName, isLoading }: SummaryCardProps) {
  return (
    <Card className='min-w-0 rounded-lg overflow-hidden bg-white dark:bg-gray-800 flex justify-center h-full'>
      <CardBody
        className={classNames('border border-gray-200 justify-between dark:border-gray-800 w-full p-6 rounded-lg text-white dark:text-emerald-100 bg-teal-600', bodyClassName)}
        >
        <div className="text-center xl:mb-0 mb-3">
          <div className={'text-center inline-block text-3xl'}>
            {icon}
          </div>
          <div>
            <p className="mb-3 text-base font-medium text-gray-50 dark:text-gray-100">
              {!isLoading ? title : <Skeleton count={1} height={20} />}
            </p>
            <p className="text-2xl font-bold leading-none text-gray-50 dark:text-gray-50">
              {value}
            </p>
          </div>
        </div>
      </CardBody>
    </Card>
  )
}

type SummaryCard2Props = {
  icon: ReactNode
  title: string
  value: string
  isLoading?: boolean
  iconContainerClassName?: string
}

function SummaryCard2({ icon, title, value, iconContainerClassName, isLoading }: SummaryCard2Props) {
  const { mode } = useContext(WindmillContext)

  if (isLoading) {
    return (
      <Skeleton
        count={2}
        height={40}
        className="dark:bg-gray-800 bg-gray-200"
        baseColor={`${mode === 'dark' ? '#010101' : '#f9f9f9'}`}
        highlightColor={`${mode === 'dark' ? '#1a1c23' : '#f8f8f8'} `}
      />
    )
  } else {
    return (
      <Card className="flex h-full">
        <CardBody className="flex items-center border border-gray-200 dark:border-gray-800 w-full rounded-lg">
          <div
            className={classNames('flex items-center justify-center p-3 rounded-full h-12 w-12 text-center mr-4 text-lg', iconContainerClassName)}
          >
            {icon}
          </div>
          <div>
            <h6 className="text-sm mb-1 font-medium text-gray-600 dark:text-gray-400">
              <span>{title}</span>
            </h6>
            <p className="text-2xl font-bold leading-none text-gray-600 dark:text-gray-200">
              {value}
            </p>
          </div>
        </CardBody>
      </Card>
    )
  }
}

export function OrdersSummary() {
  return (
    <div className='space-y-4'>
      <div className="grid gap-4 md:grid-cols-4 xl:grid-cols-4 ">
        <SummaryCard
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          bodyClassName='text-white dark:text-emerald-100 bg-teal-600'
        />
        <SummaryCard
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          bodyClassName='text-white dark:text-orange-100 bg-orange-400'
        />
        <SummaryCard
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          bodyClassName='text-white dark:text-emerald-100 bg-blue-500'
        />
        <SummaryCard
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          bodyClassName='text-white dark:text-teal-100 bg-cyan-600'
        />
      </div>
      <div className="grid gap-4 md:grid-cols-4 xl:grid-cols-4 ">
        <SummaryCard2
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          iconContainerClassName='text-orange-600 dark:text-orange-100 bg-orange-100 dark:bg-orange-500'
        />
        <SummaryCard2
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          iconContainerClassName='text-blue-600 dark:text-blue-100 bg-blue-100 dark:bg-blue-500'
        />
        <SummaryCard2
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          iconContainerClassName='text-teal-600 dark:text-teal-100 bg-teal-100 dark:bg-teal-500'
        />
        <SummaryCard2
          icon={<ImStack />}
          title='Today Orders'
          value='1.000'
          iconContainerClassName='text-emerald-600 dark:text-emerald-100 bg-emerald-100 dark:bg-emerald-500'
        />
      </div>
    </div>

  )
}
