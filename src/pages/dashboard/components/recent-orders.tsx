import { Pagination, Table, TableBody, TableCell, TableContainer, TableFooter, TableHeader, TableRow } from '@windmill/react-ui'

export function RecentOrders() {
  return (
    <TableContainer className="mb-8">
      <Table>
        <TableHeader>
          <tr>
            <TableCell>INV NO</TableCell>
            <TableCell>TM</TableCell>
            <TableCell>CUST </TableCell>
            <TableCell>MTD</TableCell>
            <TableCell>AMT</TableCell>
            <TableCell>STATUS</TableCell>
            <TableCell>ACT</TableCell>
            <TableCell className="text-right">INV</TableCell>
          </tr>
        </TableHeader>
        <TableBody>
          <TableRow>
            <TableCell className='font-semibold uppercase text-xs'>
              Inv
            </TableCell>
            <TableCell className='text-sm'>
              DT
            </TableCell>
            <TableCell className="text-xs">
              USER
            </TableCell>
            <TableCell className="text-sm font-semibold">
              METH
            </TableCell>
            <TableCell className="text-sm font-semibold">
              AMT
            </TableCell>
            <TableCell className="text-xs">
              ORDER STAT
            </TableCell>
            <TableCell className="text-center">
              SELECTSTAT
            </TableCell>
            <TableCell className="text-right flex justify-end">
              <div className="flex justify-between items-center">
                Print DLL
              </div>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
      <TableFooter>
        <Pagination
          totalResults={10}
          resultsPerPage={8}
          onChange={() => {}}
          label="Table navigation"
        />
      </TableFooter>
    </TableContainer>
  )
}
