import { AdminBodyScaffold } from 'layouts/admin-body-scaffold'
import { AdminLayout } from '../../layouts/admin-layout'
import { OrdersSummary } from './components/order-summary'
import { RecentOrders } from './components/recent-orders'

export function DashboardPage() {
  return (
    <AdminLayout activeMenuKey='dashboard'>
      <AdminBodyScaffold title="Dashboard">
        <div className='space-y-4'>
          <OrdersSummary />
          <RecentOrders />
        </div>
      </AdminBodyScaffold>
    </AdminLayout>
  )
}
