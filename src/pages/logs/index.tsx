import { InfoOutlined, ReloadOutlined } from '@ant-design/icons'
import { Button, Col, Input, List, Modal, Pagination, Row, Select, Space, Typography, message } from 'antd'
import dayjs from 'dayjs'
import { useMutation, useQuery } from 'react-query'
import { StdLayout } from '../../components/standard-layout'
import { FilterConfig, useQueryFilters } from '../../hooks/use-query-filter'
import { LogsService } from '../../services/logs'

const SORT_OPTIONS = [{ label: 'Newest', value: 'newest' }, { label: 'Oldest', value: 'oldest' }]

const FILT: Record<string, FilterConfig> = {
  search: {
    match: /.*/,
    default: ''
  },
  page: {
    match: /[0-9]+/,
    default: '1'
  },
  sort: {
    match: /(newest|oldest)/,
    default: 'newest',
    translate: value => {
      const MAP = {
        newest: { sortBy: 'createdAt', sortOrder: 'desc' },
        oldest: { sortBy: 'createdAt', sortOrder: 'asc' }
      }
      const key = value as keyof typeof MAP
      return MAP[key] || MAP.newest
    }
  }
}

type LogItemProps = {
  log: any
  afterUpdated?: any
}

function LogItem({ log, afterUpdated }: LogItemProps) {
  const retry = useMutation(() => LogsService.retryById(log.id))

  const handleRetry = async () => {
    try {
      await retry.mutateAsync()
      afterUpdated?.()
    } catch (err: any) {
      message.error(err?.message || 'Something went wrong')
    }
  }

  const handleInfo = () => {
    Modal.info({
      title: 'Detail',
      content: <Input.TextArea value={log.meta}/>,
      centered: true
    })
  }

  return (
    <List.Item
      extra={
        <Space>
          <div>{dayjs(log.createdAt).fromNow()}</div>
          <div><Button onClick={handleInfo} size='small' shape='circle' icon={<InfoOutlined />}/></div>
          <div><Button onClick={handleRetry} size='small' shape='circle' icon={<ReloadOutlined />}/></div>
        </Space>
      }
    >
      <List.Item.Meta
        title={log.group}
        description={log.message}
      />
    </List.Item>
  )
}

export function LogsPage() {
  const [filters, apiFilter, refilter] = useQueryFilters(FILT)
  const { data, refetch, isLoading } = useQuery(['logs[]', apiFilter], () => LogsService.findMany(apiFilter), { refetchInterval: 5000 })
  const logs: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleRefresh = () => refetch()

  return (
    <StdLayout
      menuProps={{ selectedKeys: ['logs'] }}
      applet={
        <Row gutter={8}>
          <Col span={12}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Search</Typography.Text>
              <Input.Search allowClear defaultValue={filters.search} onSearch={q => refilter({ search: q, page: 1 })} placeholder='Cari log...' />
            </Space>
          </Col>
          <Col span={8}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Sort</Typography.Text>
              <Select defaultValue={filters.sort} onChange={v => refilter({ sort: v })} options={SORT_OPTIONS} style={{ width: '100%' }} placeholder="Sort..." />
            </Space>
          </Col>
          <Col span={4}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Action</Typography.Text>
              <Button onClick={handleRefresh} loading={isLoading}>Refresh</Button>
            </Space>
          </Col>
        </Row>
      }
    >
      <List
        dataSource={logs}
        renderItem={log => (
          <LogItem log={log} afterUpdated={refetch} />
        )}
        footer={
          <Pagination
            pageSize={1}
            total={meta?.numPages}
            current={meta?.page || 0}
            onChange={p => refilter({ page: p })}
          />
        }
      />
    </StdLayout>
  )
}
