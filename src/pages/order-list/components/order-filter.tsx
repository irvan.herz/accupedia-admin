import { Button, Card, CardBody, Input, Select } from '@windmill/react-ui'
import dayjs from 'dayjs'
import { RangePicker } from 'rc-picker'
import generateConfigDayJS from 'rc-picker/lib/generate/dayjs'
import pickerLocaleEnUS from 'rc-picker/lib/locale/en_US'
import { useState } from 'react'

const SORT_OPTIONS = [
  {
    key: 'createdAt:desc',
    name: 'Dari yang terbaru',
    value: 'createdAt:desc'
  },
  {
    key: 'createdAt:asc',
    name: 'Dari yang terlama',
    value: 'createdAt:asc'
  },
  {
    key: 'name:asc',
    name: 'Berdasarkan nama (A - Z)',
    value: 'name:asc'
  },
  {
    key: 'name:desc',
    name: 'Berdasarkan nama (Z - A)',
    value: 'name:desc'
  }
]

const STATUS_OPTIONS = [
  {
    key: 'paid',
    name: 'Sudah dibayar',
    value: 'paid'
  },
  {
    key: 'canceled',
    name: 'Dibatalkan',
    value: 'canceled'
  },
  {
    key: 'accepted',
    name: 'Dikonfirmasi Penjual',
    value: 'accepted'
  },
  {
    key: 'packing',
    name: 'Dipacking Penjual',
    value: 'packing'
  },
  {
    key: 'delivery',
    name: 'Dalam pengiriman',
    value: 'delivery'
  },
  {
    key: 'delivered',
    name: 'Diterima pembeli',
    value: 'delivered'
  },
  {
    key: 'completed',
    name: 'Pesanan Selesai',
    value: 'completed'
  }
]

type OrderFilterProps = {
  value: any,
  onChange: (v: any) => void
}
export function OrderFilter({ value, onChange }:OrderFilterProps) {
  const [search, setSearch] = useState('')
  const [status, setStatus] = useState<string | undefined>()
  const [sort, setSort] = useState('createdAt:desc')

  const handleFilter = () => {
    const newValue = {} as any
    if (search) newValue.search = search
    if (status) newValue.status = search
    if (sort) {
      const [sortBy, sortOrder] = sort.split(':')
      newValue.sortBy = sortBy
      newValue.sortOrder = sortOrder
    }
    onChange(newValue)
  }

  const handleResetFilter = () => {
    setSearch('')
    onChange({})
  }

  return (
    <Card className="min-w-0 shadow-xs overflow-hidden bg-white dark:bg-gray-800 rounded-t-lg rounded-0 mb-4">
      <CardBody>
        <div className="grid grid-cols-12 gap-4" >
          <Input
            crossOrigin=''
            css=''
            className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 border-transparent focus:bg-white col-span-6"
            type="search"
            name="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Cari nama pelanggan..."/>
          <Select
            css=''
            value={status}
            onChange={e => setStatus(e.target.value)}
            className='col-span-3'>
            <option selected={!status} value={0}>Semua Status</option>
            {STATUS_OPTIONS.map(o => (
              <option key={o.key} value={o.value} selected={o.value === sort}>{o.name}</option>
            ))}
          </Select>
          <Select
            css=''
            value={sort}
            onChange={e => setSort(e.target.value)}
            className='col-span-3'>
            <option disabled>Urutan...</option>
            {SORT_OPTIONS.map(sort => (
              <option key={sort.key} value={sort.value}>{sort.name}</option>
            ))}
          </Select>
          <RangePicker
            showNow
            disabledDate={d => d.isAfter(dayjs(), 'day')}
            allowClear
            placeholder={['Rentang waktu dari', 'Rentang waktu sampai']}
            className='col-span-6'
            generateConfig={generateConfigDayJS}
            locale={pickerLocaleEnUS}/>
          <Button layout='primary' className='col-span-3' onClick={handleFilter}>Cari</Button>
          <Button layout='outline' className='col-span-3' onClick={handleResetFilter}>Reset</Button>
        </div>
      </CardBody>
    </Card>
  )
}
