import { Textarea } from '@windmill/react-ui'
import { Input } from 'antd'
import { FormItem } from 'components/form/form-item'
import { Form } from 'react-hook-form'

export function OrderForm() {
  return (
    <Form>
      <FormItem
        name='name'
        label='Nama'
        rules={{ required: { value: true, message: 'Nama tidak boleh kosong' } }}>
        <Input
          className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white"
          type="text"
          placeholder='Nama kategori...' />
      </FormItem>
      <FormItem
        name='description'
        label='Deskripsi'
        rules={{ required: { value: true, message: 'Deskripsi tidak boleh kosong' } }}>
        <Textarea
          className='border p-2 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white'
          rows={5}
          css=''
          placeholder='Deskripsi...' />
      </FormItem>
    </Form>
  )
}
