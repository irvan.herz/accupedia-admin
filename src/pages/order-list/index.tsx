import { Badge, Pagination, Table, TableBody, TableCell, TableContainer, TableFooter, TableHeader, TableRow } from '@windmill/react-ui'
import { useOrders } from 'hooks/services/orders'
import { AdminBodyScaffold } from 'layouts/admin-body-scaffold'
import { AdminLayout } from 'layouts/admin-layout'
import { useState } from 'react'
import { DeleteOrderButton } from './components/delete-order-button'
import { EmptyOrder } from './components/empty-order'
import { OrderFilter } from './components/order-filter'

type OrderTableRowProps = {
  order: any
  afterDeleted: () => void
}

function OrderTableRow({ order, afterDeleted }:OrderTableRowProps) {
  return (
    <TableRow>
      <TableCell className='text-sm'>
        {order.name}
      </TableCell>
      <TableCell className='text-sm'>
        {order.description}
      </TableCell>
      <TableCell>
        <Badge type="success">{order.status}</Badge>
      </TableCell>
      <TableCell>
        <DeleteOrderButton order={order} afterDeleted={afterDeleted} />
      </TableCell>
    </TableRow>
  )
}

export function OrderListPage() {
  const [filter, setFilter] = useState({})
  const { data, refetch } = useOrders(filter)
  const categories: any[] = data?.data || []
  const meta = data?.meta || {}

  return (
    <AdminLayout activeMenuKey='orders'>
      <AdminBodyScaffold title="Daftar Pesanan">
        <OrderFilter value={filter} onChange={setFilter} />
        {!categories.length && (
          <EmptyOrder />
        )}
        {!!categories.length && (
          <TableContainer className="mb-8 rounded-b-lg">
            <Table>
              <TableHeader>
                <tr>
                  <TableCell>Nama</TableCell>
                  <TableCell>Deskripsi</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell className="text-right">Aksi</TableCell>
                </tr>
              </TableHeader>
              <TableBody>
                {categories.map(cat => (
                  <OrderTableRow key={cat.id} order={cat} afterDeleted={refetch} />
                ))}
              </TableBody>
            </Table>
            <TableFooter>
              <Pagination
                totalResults={meta?.numItems || 1}
                resultsPerPage={meta?.limit || 1}
                onChange={() => {}}
                label="Order Page Navigation"/>
            </TableFooter>
          </TableContainer>
        )}

      </AdminBodyScaffold>
    </AdminLayout>
  )
}
