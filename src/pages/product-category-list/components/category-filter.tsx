import { Button, Card, CardBody, Input } from '@windmill/react-ui'
import { useState } from 'react'

type CategoryFilterProps = {
  value: any,
  onChange: (v: any) => void
}
export function CategoryFilter({ value, onChange }:CategoryFilterProps) {
  const [search, setSearch] = useState('')

  const handleFilter = () => {
    const newValue = {} as any
    if (search) newValue.search = search
    onChange(newValue)
  }

  const handleResetFilter = () => {
    setSearch('')
    onChange({})
  }

  return (
    <Card className="min-w-0 shadow-xs overflow-hidden bg-white dark:bg-gray-800 rounded-t-lg rounded-0 mb-4">
      <CardBody>
        <div className="grid grid-cols-12 gap-4" >
          <Input
            crossOrigin=''
            css=''
            className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 border-transparent focus:bg-white col-span-6"
            type="search"
            name="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Cari kategori produk..."/>
          <Button layout='primary' className='col-span-3' onClick={handleFilter}>Cari</Button>
          <Button layout='outline' className='col-span-3' onClick={handleResetFilter}>Reset</Button>
        </div>
      </CardBody>
    </Card>
  )
}
