import { Button, Modal, ModalBody, ModalFooter } from '@windmill/react-ui'
import { useDeleteProductCategory } from 'hooks/services/products'
import { useState } from 'react'
import { FiTrash2 } from 'react-icons/fi'
import { toast } from 'react-toastify'

type DeleteCategoryButtonProps = {
  category: any
  afterDeleted?: () => void
}
export function DeleteCategoryButton({ category, afterDeleted }: DeleteCategoryButtonProps) {
  const destroyer = useDeleteProductCategory()
  const [open, setOpen] = useState(false)

  const handleOpen = () => setOpen(true)

  const handleClose = () => setOpen(false)

  const handleDelete = async () => {
    try {
      await destroyer.mutateAsync({ id: category.id })
      handleClose()
      afterDeleted?.()
    } catch (err: any) {
      toast.error(err?.message || 'Terjadi kesalahan', {
        position: 'top-center',
        autoClose: 3000
      })
    }
  }

  return (
    <>
      <button
        onClick={handleOpen}
        className="p-2 cursor-pointer text-gray-400 hover:text-red-600 focus:outline-none">
        <p className="text-xl">
          <FiTrash2 />
        </p>
      </button>
      <Modal isOpen={open} onClose={handleClose}>
        <ModalBody className="text-center p-6">
          <span className="inline-block text-4xl mb-4 text-red-500">
            <FiTrash2 />
          </span>
          <h2 className="text-xl mb-1 font-bold">Konfirmasi</h2>
          <p>Apakah Anda yakin ingin menghapus kategori <span className='font-bold'>{category.name}</span>?</p>
        </ModalBody>
        <ModalFooter >
          <div className="flex w-full">
            <Button
              className='flex-1'
              layout="outline"
              onClick={handleClose}>
              Batal
            </Button>
            <Button
              className='flex-1'
              layout='primary'
              onClick={handleDelete}>
              Hapus
            </Button>
          </div>
        </ModalFooter>
      </Modal>
    </>
  )
}
