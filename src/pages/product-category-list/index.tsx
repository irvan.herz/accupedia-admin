import { Badge, Button, Card, CardBody, Pagination, Table, TableBody, TableCell, TableContainer, TableFooter, TableHeader, TableRow } from '@windmill/react-ui'
import { useProductCategories } from 'hooks/services/products'
import { AdminBodyScaffold } from 'layouts/admin-body-scaffold'
import { AdminLayout } from 'layouts/admin-layout'
import { useState } from 'react'
import { AddCategoryButton } from './components/add-category-button'
import { CategoryFilter } from './components/category-filter'
import { DeleteCategoryButton } from './components/delete-category-button'
import { EmptyCategory } from './components/empty-category'

type CategoryTableRowProps = {
  category: any
  afterDeleted: () => void
}

function CategoryTableRow({ category, afterDeleted }:CategoryTableRowProps) {
  return (
    <TableRow>
      <TableCell className='text-sm'>
        {category.name}
      </TableCell>
      <TableCell className='text-sm'>
        {category.description}
      </TableCell>
      <TableCell>
        <Badge type="success">{category.status}</Badge>
      </TableCell>
      <TableCell className='text-right'>
        <DeleteCategoryButton category={category} afterDeleted={afterDeleted} />
      </TableCell>
    </TableRow>
  )
}

export function ProductCategoryListPage() {
  const [filter, setFilter] = useState({})
  const { data, refetch } = useProductCategories(filter)
  const categories: any[] = data?.data || []
  const meta = data?.meta || {}

  return (
    <AdminLayout activeMenuKey='catalog' expandedMenuKey='catalog'>
      <AdminBodyScaffold title="Daftar Kategori">
        <Card className="min-w-0 shadow-xs overflow-hidden bg-white dark:bg-gray-800 mb-5">
          <CardBody>
            <div className='py-3 flex'>
              <Button layout='outline'>Impor</Button>
              <AddCategoryButton afterCreated={refetch} />
            </div>
          </CardBody>
        </Card>
        <CategoryFilter value={filter} onChange={setFilter} />
        {!categories.length && (
          <EmptyCategory />
        )}
        {!!categories.length && (
          <TableContainer className="mb-8 rounded-b-lg">
            <Table>
              <TableHeader>
                <tr>
                  <TableCell>Nama</TableCell>
                  <TableCell>Deskripsi</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell className="text-right">Aksi</TableCell>
                </tr>
              </TableHeader>
              <TableBody>
                {categories.map(cat => (
                  <CategoryTableRow key={cat.id} category={cat} afterDeleted={refetch} />
                ))}
              </TableBody>
            </Table>
            <TableFooter>
              <Pagination
                totalResults={meta?.numItems || 1}
                resultsPerPage={meta?.limit || 1}
                onChange={() => {}}
                label="Category Page Navigation"/>
            </TableFooter>
          </TableContainer>
        )}

      </AdminBodyScaffold>
    </AdminLayout>
  )
}
