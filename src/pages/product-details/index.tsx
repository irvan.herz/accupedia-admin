import {
  Badge
} from '@windmill/react-ui'
import { AdminLayout } from 'layouts/admin-layout'

export function ProductDetailsPage () {
  return (
    <AdminLayout activeMenuKey='catalog' expandedMenuKey='catalog'>
      <div className="inline-block overflow-y-auto h-full align-middle transition-all transform">
        <div className="flex flex-col lg:flex-row md:flex-row w-full overflow-hidden">
          <div className="flex-shrink-0 flex items-center justify-center h-auto">
            img
          </div>
          <div className="w-full flex flex-col p-5 md:p-8 text-left">
            <div className="mb-5 block ">
              <div className="font-serif font-semibold py-1 text-sm">
                <p className="text-sm text-gray-500 pr-4">
                  <span className="text-green-400">
                    status
                  </span>
                </p>
              </div>
              <h2 className="text-heading text-lg md:text-xl lg:text-2xl font-semibold font-serif dark:text-gray-400">
                VAL
              </h2>
              <p className="uppercase font-serif font-medium text-gray-500 dark:text-gray-400 text-sm">
                SKU
                <span className="font-bold text-gray-500 dark:text-gray-500">
                  {/* {data?._id !== undefined && data?._id.substring(18, 24)} */}
                  SKU 0
                </span>
              </p>
            </div>
            <div className="font-serif product-price font-bold dark:text-gray-400">
              <span className="inline-block text-2xl">
                PRICE
              </span>
            </div>
            <div className="mb-3">
              <Badge type="danger">
                <span className="font-bold">STOCKOUT</span>{' '}
              </Badge>
              <span className="text-sm text-gray-500 dark:text-gray-400 font-medium pl-4">
                Quantity: 0
              </span>
            </div>
            <p className="text-sm leading-6 text-gray-500 dark:text-gray-400 md:leading-7">
              DESC
            </p>
            <div className="flex flex-col mt-4">
              <p className="font-serif font-semibold py-1 text-gray-500 text-sm">
                <span className="text-gray-700 dark:text-gray-400">
                  Category
                </span>{' '}
                CATNAME
              </p>
              <div className="flex flex-row">
                <span className="bg-gray-200 mr-2 border-0 text-gray-500 rounded-full inline-flex items-center justify-center px-2 py-1 text-xs font-semibold font-serif mt-2 dark:bg-gray-700 dark:text-gray-300">
                  tag
                </span>
              </div>
            </div>
            <div className="mt-6">
              <button
                className="cursor-pointer leading-5 transition-colors duration-150 font-medium text-sm focus:outline-none px-5 py-2 rounded-md text-white bg-green-500 border border-transparent active:bg-green-600 hover:bg-green-600 focus:ring focus:ring-purple-300">
                EditProd
              </button>
            </div>
          </div>
        </div>
      </div>
    </AdminLayout>
  )
};
