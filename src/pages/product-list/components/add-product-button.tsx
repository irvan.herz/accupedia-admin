import { Button } from '@windmill/react-ui'
import { useCreateProduct } from 'hooks/services/products'
import { useWindowDimensions } from 'hooks/use-window-dimensions'
import Drawer from 'rc-drawer'
import { useState } from 'react'
import { FormProvider, SubmitErrorHandler, SubmitHandler, useForm } from 'react-hook-form'
import { FiPlus, FiX } from 'react-icons/fi'
import { toast } from 'react-toastify'
import { ProductForm } from './product-form'
type AddProductButtonProps = {
  afterCreated?: () => void
}
export function AddProductButton({ afterCreated }:AddProductButtonProps) {
  const creator = useCreateProduct()
  const [open, setOpen] = useState(false)
  const windowDimensions = useWindowDimensions()
  const modalWidth = windowDimensions.width > 700 ? 700 : '100%'

  const form = useForm()

  const handleToggle = () => setOpen(!open)

  const handleClose = () => setOpen(false)

  const handleProcessSubmit: SubmitHandler<any> = async (values) => {
    try {
      await creator.mutateAsync({ data: values })
      handleClose()
      afterCreated?.()
    } catch (err: any) {
      toast.error(err?.message || 'Terjadi kesalahan', {
        position: 'top-center',
        autoClose: 3000
      })
    }
  }

  const handleProcessSubmitError: SubmitErrorHandler<any> = (val) => {
    toast.error('Periksa kembali formulir Anda', {
      position: 'top-center',
      autoClose: 3000
    })
  }

  return (
    <>
      <Button
        onClick={handleToggle}
        className="w-full rounded-md h-12">
        <span className="mr-2"><FiPlus /> </span>Tambah Produk
      </Button>
      <Drawer
        prefixCls='drawer'
        open={open}
        destroyOnClose
        getContainer={() => document.getElementById('modals-container')!}
        onClose={handleToggle}
        placement={'right'}
        width={modalWidth}
      >
        <div className='flex flex-col w-full h-full'>
          <div className="flex-none relative p-6 border-b border-gray-100 bg-gray-50 dark:border-gray-700 dark:bg-gray-800 dark:text-gray-300">
            <div>
              <h4 className="text-xl font-medium dark:text-gray-300">Tambah Produk</h4>
              <p className="mb-0 text-sm dark:text-gray-300">Masukkan detail produk yang ingin Anda buat di bawah ini</p>
            </div>
            <button
              onClick={handleToggle}
              className="absolute focus:outline-none text-red-500 hover:bg-red-100 hover:text-gray-700 transition-colors duration-150 bg-white shadow-md mr-6 mt-6 right-0 top-0 w-10 h-10 rounded-full block text-center">
              <FiX className="mx-auto" />
            </button>
          </div>
          <div className="flex-1 relative">
            <div className='absolute w-full h-full left-0 top-0 overflow-y-auto p-6'>
              <FormProvider {...form}>
                <ProductForm />
              </FormProvider>
            </div>
          </div>
          <div className="flex-none p-6 border-t border-gray-100 bg-gray-50 dark:border-gray-700 dark:bg-gray-800 dark:text-gray-300 flex">
            <Button layout='outline' className='flex-1' onClick={handleToggle} disabled={creator.isLoading}>Batal</Button>
            <Button layout='primary' className='flex-1' onClick={form.handleSubmit(handleProcessSubmit, handleProcessSubmitError)} disabled={creator.isLoading}>Tambahkan</Button>
          </div>
        </div>
      </Drawer>
    </>
  )
}
