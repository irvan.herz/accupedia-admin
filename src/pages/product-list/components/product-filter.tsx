import { Button, Card, CardBody, Input, Select } from '@windmill/react-ui'
import { useState } from 'react'
import { SelectCategoryId } from './select-category'

const SORT_OPTIONS = [
  {
    key: 'createdAt:desc',
    name: 'Dari yang terbaru',
    value: 'createdAt:desc'
  },
  {
    key: 'createdAt:asc',
    name: 'Dari yang terlama',
    value: 'createdAt:asc'
  },
  {
    key: 'name:asc',
    name: 'Berdasarkan nama (A - Z)',
    value: 'name:asc'
  },
  {
    key: 'name:desc',
    name: 'Berdasarkan nama (Z - A)',
    value: 'name:desc'
  }
]

type ProductFilterProps = {
  value: any,
  onChange: (v: any) => void
}
export function ProductFilter({ value, onChange }:ProductFilterProps) {
  const [sort, setSort] = useState('createdAt:desc')
  const [categoryId, setCategoryId] = useState(0)
  const [search, setSearch] = useState('')

  const handleFilter = () => {
    const newValue = {} as any
    if (search) newValue.search = search
    if (categoryId) newValue.categoryId = categoryId
    if (sort) {
      const [sortBy, sortOrder] = sort.split(':')
      newValue.sortBy = sortBy
      newValue.sortOrder = sortOrder
    }
    onChange(newValue)
  }

  const handleResetFilter = () => {
    setSearch('')
    setSort('')
    setCategoryId(0)
    onChange({})
  }

  return (
    <Card className="min-w-0 shadow-xs overflow-hidden bg-white dark:bg-gray-800 rounded-t-lg rounded-0 mb-4">
      <CardBody>
        <div className="grid grid-cols-12 gap-4" >
          <Input
            crossOrigin=''
            css=''
            className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 border-transparent focus:bg-white col-span-4"
            type="search"
            name="search"
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Cari produk..."/>
          <SelectCategoryId
            value={categoryId}
            onChange={setCategoryId}
            className='col-span-3'/>
          <Select
            css=''
            value={sort}
            onChange={e => setSort(e.target.value)}
            className='col-span-3'>
            <option disabled>Urutan...</option>
            {SORT_OPTIONS.map(sort => (
              <option key={sort.key} value={sort.value}>{sort.name}</option>
            ))}
          </Select>
          <Button layout='primary' className='col-span-1' onClick={handleFilter}>Cari</Button>
          <Button layout='outline' className='col-span-1' onClick={handleResetFilter}>Reset</Button>
        </div>
      </CardBody>
    </Card>
  )
}
