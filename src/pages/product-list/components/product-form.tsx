import { Textarea } from '@windmill/react-ui'
import { Input } from 'antd'
import { FormItem } from 'components/form/form-item'
import { Form } from 'react-hook-form'
import { SelectCategoryId } from './select-category'

export function ProductForm() {
  return (
    <Form>
      <FormItem name='title' label='Nama Produk' rules={{ required: { value: true, message: 'Nama produk tidak boleh kosong' } }}>
        <Input
          className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white"
          type="text"
          placeholder='Nama produk...' />
      </FormItem>
      <FormItem name='description' label='Deskripsi'>
        <Textarea
          className='border p-2 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white'
          rows={5}
          css=''
          placeholder='Deskripsi produk...' />
      </FormItem>
      <FormItem name='sku' label='SKU' rules={{ required: { value: true, message: 'Kode SKU tidak boleh kosong' } }}>
        <Input
          className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white"
          placeholder='Kode SKU...' />
      </FormItem>
      <FormItem name='categoryId' label='Kategori Produk' rules={{ required: { value: true, message: 'Kategori produk tidak boleh kosong' } }}>
        <SelectCategoryId />
      </FormItem>
      <FormItem name='price' label='Harga' rules={{ required: { value: true, message: 'Harga produk tidak boleh kosong' } }}>
        <Input
          className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white"
          type='number'
          min={0}
          step={100}
          placeholder='Harga produk...' />
      </FormItem>
      <FormItem name='quantity' label='Jumlah Stok' rules={{ required: { value: true, message: 'Stok produk tidak boleh kosong' } }}>
        <Input
          className="border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white"
          min={0}
          type='number'
          placeholder='Jumlah stok...' />
      </FormItem>
    </Form>
  )
}
