import { Select } from '@windmill/react-ui'
import { useProductCategories } from 'hooks/services/products'
import { useCustomComponent } from 'hooks/use-custom-component'

type SelectCategoryIdProps = {
  value?: any,
  defaultValue?: any
  onChange?: (v: any) => void
  className?:string
}
export function SelectCategoryId({ value, defaultValue, onChange, className }:SelectCategoryIdProps) {
  const [computedValue, triggerChange] = useCustomComponent({ value, defaultValue, onChange })
  const { data } = useProductCategories({})
  const categories: any[] = data?.data || []

  const handleChange = (e: any) => {
    triggerChange(e.target.value)
  }

  return (
    <Select css='' className={className} value={computedValue} onChange={handleChange} placeholder='Pilih kategori...'>
      <option>Pilih kategori...</option>
      {categories.map(cat => (
        <option key={cat.id} value={cat.id}>{cat.name}</option>
      ))}
    </Select>
  )
}
