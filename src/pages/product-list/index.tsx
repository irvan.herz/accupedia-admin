import { Badge, Button, Card, CardBody, Pagination, Table, TableBody, TableCell, TableContainer, TableFooter, TableHeader, TableRow } from '@windmill/react-ui'
import { useProducts } from 'hooks/services/products'
import { AdminBodyScaffold } from 'layouts/admin-body-scaffold'
import { AdminLayout } from 'layouts/admin-layout'
import { useState } from 'react'
import { AddProductButton } from './components/add-product-button'
import { DeleteProductButton } from './components/delete-product-button'
import { EmptyProduct } from './components/empty-product'
import { ProductFilter } from './components/product-filter'

type ProductTableRowProps = {
  product: any
  afterDeleted: () => void
}

function ProductTableRow({ product, afterDeleted }:ProductTableRowProps) {
  return (
    <TableRow>
      <TableCell className='text-sm font-bold'>
        {product.sku}
      </TableCell>
      <TableCell className='text-sm'>
        {product.title}
      </TableCell>
      <TableCell className='text-sm'>
        {product.category?.name || <i>Uncategorized</i>}
      </TableCell>
      <TableCell className="text-sm font-semibold">
        {Number(+product?.price).toFixed(0)}
      </TableCell>
      <TableCell className="text-sm">
        {product.quantity || 0}
      </TableCell>
      <TableCell>
        <Badge type="success">Selling</Badge>
      </TableCell>
      <TableCell className='text-right'>
        <DeleteProductButton product={product} afterDeleted={afterDeleted} />
      </TableCell>
    </TableRow>
  )
}

export function ProductListPage() {
  const [filter, setFilter] = useState({})
  const { data, refetch } = useProducts(filter)
  const products: any[] = data?.data || []
  const meta = data?.meta || {}

  return (
    <AdminLayout activeMenuKey='catalog' expandedMenuKey='catalog'>
      <AdminBodyScaffold title="Daftar Produk">
        <Card className="min-w-0 shadow-xs overflow-hidden bg-white dark:bg-gray-800 mb-5">
          <CardBody>
            <div className='py-3 flex'>
              <Button layout='outline'>Impor</Button>
              <AddProductButton afterCreated={refetch} />
            </div>
          </CardBody>
        </Card>
        <ProductFilter value={filter} onChange={setFilter} />
        {!products.length && (
          <EmptyProduct />
        )}
        {!!products.length && (
          <TableContainer className="mb-8 rounded-b-lg">
            <Table>
              <TableHeader>
                <tr>
                  <TableCell>SKU</TableCell>
                  <TableCell>Nama Produk</TableCell>
                  <TableCell>Kategori</TableCell>
                  <TableCell>Harga</TableCell>
                  <TableCell>Stok</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell className="text-right">Aksi</TableCell>
                </tr>
              </TableHeader>
              <TableBody>
                {products.map(cat => (
                  <ProductTableRow key={cat.id} product={cat} afterDeleted={refetch} />
                ))}
              </TableBody>
            </Table>
            <TableFooter>
              <Pagination
                totalResults={meta?.numItems || 1}
                resultsPerPage={meta?.limit || 1}
                onChange={() => {}}
                label="Product Page Navigation"/>
            </TableFooter>
          </TableContainer>
        )}
      </AdminBodyScaffold>
    </AdminLayout>
  )
}
