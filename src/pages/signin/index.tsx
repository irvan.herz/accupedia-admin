import { Button, Input } from '@windmill/react-ui'
import { FormItem } from 'components/form/form-item'
import { RouteGuard } from 'components/route-guard'
import { AuthContext } from 'contexts/auth-context'
import { useSignin } from 'hooks/services/auth'
import { useContext } from 'react'
import { FormProvider, SubmitErrorHandler, SubmitHandler, useForm } from 'react-hook-form'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'

export function SigninPage() {
  const auth = useContext(AuthContext)
  const signin = useSignin()
  const form = useForm()

  const handleProcessSubmit: SubmitHandler<any> = async (values) => {
    try {
      const result = await signin.mutateAsync({ data: values })
      const { token, refreshToken } = result.meta
      auth.setToken(token, refreshToken)
    } catch (err: any) {
      toast.error(err?.message || 'Terjadi kesalahan', {
        position: 'top-center',
        autoClose: 3000
      })
    }
  }

  const handleProcessSubmitError: SubmitErrorHandler<any> = (val) => {
    toast.error('Periksa kembali formulir Anda', {
      position: 'top-center',
      autoClose: 3000
    })
  }

  return (
    <RouteGuard require='unauthenticated'>
      <div className="flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900">
        <div className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800">
          <div className="flex flex-col overflow-y-auto md:flex-row">
            <div className="h-32 md:h-auto md:w-1/2">
              <img
                aria-hidden="true"
                className="object-cover w-full h-full dark:hidden"
                src={`${process.env.PUBLIC_URL}/assets/images/login-office.jpeg`}
                alt="Office"
              />
              <img
                aria-hidden="true"
                className="hidden object-cover w-full h-full dark:block"
                src={`${process.env.PUBLIC_URL}/assets/images/login-office-dark.jpeg`}
                alt="Office"
              />
            </div>
            <main className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
              <div className="w-full">
                <h1 className="mb-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
                  Login
                </h1>
                <FormProvider {...form}>
                  <form>
                    <FormItem
                      rules={{ required: { value: true, message: 'Email atau username tidak boleh kosong' } }}
                      label='Email'
                      name='username'
                      labelClassName="block">
                      <Input
                        className='border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white'
                        type='email'
                        crossOrigin='' css=''
                        placeholder="john@doe.com" />
                    </FormItem>
                    <FormItem
                      rules={{ required: { value: true, message: 'Kata sandi tidak boleh kosong' } }}
                      label='Password'
                      name='password'
                      labelClassName="block">
                      <Input
                        className='border h-12 text-sm focus:outline-none block w-full bg-gray-100 dark:bg-white border-transparent focus:bg-white'
                        type='password' crossOrigin='' css=''
                        placeholder="**********" />
                    </FormItem>
                    <Button
                      onClick={form.handleSubmit(handleProcessSubmit, handleProcessSubmitError)}
                      type="submit"
                      className="mt-4 h-12 w-full">
                      Login
                    </Button>
                  </form>
                </FormProvider>
                <p className="mt-4">
                  <Link
                    className="text-sm font-medium text-green-500 dark:text-green-400 hover:underline"
                    to="/forgot-password">
                    Forgot Password
                  </Link>
                </p>
                <p className="mt-1">
                  <Link
                    className="text-sm font-medium text-green-500 dark:text-green-400 hover:underline"
                    to="/signup">
                    Create Account
                  </Link>
                </p>
              </div>
            </main>
          </div>
        </div>
      </div>
    </RouteGuard>
  )
};
