import { Button, Col, Input, List, Pagination, Row, Select, Space, Typography } from 'antd'
import dayjs from 'dayjs'
import { useQuery } from 'react-query'
import { StdLayout } from '../../components/standard-layout'
import { FilterConfig, useQueryFilters } from '../../hooks/use-query-filter'
import { TokopediaService } from '../../services/tokopedia'
import { TokopediaAddButton } from './tokopedia-add-button'
import { TokopediaEditButton } from './tokopedia-edit-button'

const SORT_OPTIONS = [{ label: 'Newest', value: 'newest' }, { label: 'Oldest', value: 'oldest' }]

const FILT: Record<string, FilterConfig> = {
  search: {
    match: /.*/,
    default: ''
  },
  page: {
    match: /[0-9]+/,
    default: '1'
  },
  sort: {
    match: /(newest|oldest)/,
    default: 'newest',
    translate: value => {
      const MAP = {
        newest: { sortBy: 'createdAt', sortOrder: 'desc' },
        oldest: { sortBy: 'createdAt', sortOrder: 'asc' }
      }
      const key = value as keyof typeof MAP
      return MAP[key] || MAP.newest
    }
  }
}

type TokopediaItemProps = {
  tokopedia: any
  afterUpdated: any
}

function TokopediaItem({ tokopedia, afterUpdated }: TokopediaItemProps) {
  return (
    <List.Item extra={dayjs(tokopedia.createdAt).fromNow()}>
      <List.Item.Meta
        title={tokopedia.email}
        description={
          <Space direction='vertical' size='small'>
            <div>App ID: {tokopedia.appId ? `${tokopedia.appId}` : <i>Belum set</i>}</div>
            <div>Shop ID: {tokopedia.shopId ? `${tokopedia.shopId}` : <i>Belum set</i>}</div>
            <div><TokopediaEditButton tokopedia={tokopedia} afterUpdated={afterUpdated}><Button size='small'>Edit</Button></TokopediaEditButton></div>
          </Space>
        }
      />
    </List.Item>
  )
}

export function TokopediaPage() {
  const [filters, apiFilter, refilter] = useQueryFilters(FILT)
  const { data, refetch, isLoading } = useQuery(['tokopedia[]', apiFilter], () => TokopediaService.findMany(apiFilter))
  const logs: any[] = data?.data || []
  const meta = data?.meta || {}

  const handleRefresh = () => refetch()

  return (
    <StdLayout
      menuProps={{ selectedKeys: ['tokopedia'] }}
      applet={
        <Row gutter={8}>
          <Col span={12}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Search</Typography.Text>
              <Input.Search allowClear defaultValue={filters.search} onSearch={q => refilter({ search: q, page: 1 })} placeholder='Cari akun tokped...' />
            </Space>
          </Col>
          <Col span={8}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Sort</Typography.Text>
              <Select defaultValue={filters.sort} onChange={v => refilter({ sort: v })} options={SORT_OPTIONS} style={{ width: '100%' }} placeholder="Sort..." />
            </Space>
          </Col>
          <Col span={4}>
            <Space direction='vertical' style={{ width: '100%' }}>
              <Typography.Text>Action</Typography.Text>
              <Button onClick={handleRefresh} loading={isLoading}>Refresh</Button>
            </Space>
          </Col>
        </Row>
      }
      bottomApplet={
        <TokopediaAddButton afterCreated={refetch}>
          <Button type='primary'>Tambah Link Tokopedia</Button>
        </TokopediaAddButton>
      }
    >
      <List
        dataSource={logs}
        renderItem={tokopedia => (
          <TokopediaItem tokopedia={tokopedia} afterUpdated={refetch} />
        )}
        footer={
          <Pagination
            pageSize={1}
            total={meta?.numPages}
            current={meta?.page || 0}
            onChange={p => refilter({ page: p })}
          />
        }
      />
    </StdLayout>
  )
}
