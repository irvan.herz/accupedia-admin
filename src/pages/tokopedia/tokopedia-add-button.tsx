import { Form, FormProps, Modal, message } from 'antd'
import { ReactElement, cloneElement, useState } from 'react'
import { useMutation } from 'react-query'
import { TokopediaService } from '../../services/tokopedia'
import { TokopediaForm } from './tokopedia-form'

type TokopediaAddButtonProps = {
  children: ReactElement
  afterCreated?: any
}

export function TokopediaAddButton({ children, afterCreated }: TokopediaAddButtonProps) {
  const [open, setOpen] = useState(false)
  const [form] = Form.useForm()
  const creator = useMutation(x => TokopediaService.create(x))

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleFinish: FormProps['onFinish'] = async (values) => {
    try {
      await creator.mutateAsync(values)
      afterCreated?.()
      handleClose()
    } catch (err:any) {
      message.error(err?.message || 'Terjadi kesalahan')
    }
  }

  const handleFinishFailed: FormProps['onFinishFailed'] = (errorInfo) => {
    message.error('Cek ulang form')
  }

  return (
    <>
      {cloneElement(children, { onClick: handleClick, loading: creator.isLoading })}
      <Modal
        title="Tambah Link Tokopedia"
        open={open}
        centered
        onCancel={handleClose}
        onOk={form.submit}
      >
        <Form form={form} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} onFinish={handleFinish} onFinishFailed={handleFinishFailed}>
          <TokopediaForm />
        </Form>
      </Modal>
    </>
  )
}
