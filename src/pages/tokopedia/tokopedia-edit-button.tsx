import { Form, FormProps, Modal, message } from 'antd'
import { ReactElement, cloneElement, useEffect, useState } from 'react'
import { useMutation } from 'react-query'
import { TokopediaService } from '../../services/tokopedia'
import { TokopediaForm } from './tokopedia-form'

type TokopediaEditButtonProps = {
  tokopedia: any
  children: ReactElement
  afterUpdated?: any
}

export function TokopediaEditButton({ tokopedia, children, afterUpdated }: TokopediaEditButtonProps) {
  const [open, setOpen] = useState(false)
  const [form] = Form.useForm()
  const updater = useMutation(x => TokopediaService.updateById(tokopedia.id, x))

  useEffect(() => {
    if (open) form.setFieldsValue(tokopedia)
  }, [open, tokopedia])

  const handleClick = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleFinish: FormProps['onFinish'] = async (values) => {
    try {
      await updater.mutateAsync(values)
      afterUpdated?.()
      handleClose()
    } catch (err:any) {
      message.error(err?.message || 'Terjadi kesalahan')
    }
  }

  const handleFinishFailed: FormProps['onFinishFailed'] = (errorInfo) => {
    message.error('Cek ulang form')
  }

  return (
    <>
      {cloneElement(children, { onClick: handleClick, loading: updater.isLoading })}
      <Modal
        title="Edit Link Tokopedia"
        open={open}
        centered
        onOk={form.submit}
        onCancel={handleClose}
      >
        <Form form={form} labelCol={{ span: 24 }} wrapperCol={{ span: 24 }} onFinish={handleFinish} onFinishFailed={handleFinishFailed}>
          <TokopediaForm />
        </Form>
      </Modal>
    </>
  )
}
