import { Form, Input } from 'antd'
import { AccurateIdInput } from '../../components/accurate-picker'

export function TokopediaForm() {
  return (
    <Form.Provider>
      <Form.Item
        label="Email"
        name="email"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="App ID"
        name="appId"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Shop ID"
        name="shopId"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Client ID"
        name="clientId"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Client Secret"
        name="clientSecret"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Access Token"
        name="accessToken"
      >
        <Input/>
      </Form.Item>
      <Form.Item
        label="Link Accurate"
        help="Pilih target akun accurate yang sudah di link. Pesanan baru akan dibuatkan sales invoice dan receipt ke target"
        name="accurateId"
      >
        <AccurateIdInput />
      </Form.Item>
    </Form.Provider>
  )
}
