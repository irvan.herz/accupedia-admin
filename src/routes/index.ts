import { DashboardPage } from '../pages/dashboard'

export const mainRoutes = [
  {
    path: '/dashboard',
    component: DashboardPage
  }
]
