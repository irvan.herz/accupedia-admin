import {
  FiCompass,
  FiGrid,
  FiSlack
} from 'react-icons/fi'

/**
 * ⚠ These are used just to render the Sidebar!
 * You can include any link here, local or external.
 *
 * If you're looking to actual Router routes, go to
 * `routes/index.js`
 */
export const sidebarRoutes = [
  {
    key: 'dashboard',
    path: '/dashboard', // the url
    icon: FiGrid, // icon
    name: 'Dashboard' // name that appear in Sidebar
  },

  {
    key: 'catalog',
    icon: FiSlack,
    name: 'Catalog',
    routes: [
      {
        path: '/products',
        name: 'Products'
      },
      {
        path: '/products/categories',
        name: 'Categories'
      }
    ]
  },
  {
    key: 'orders',
    path: '/orders',
    icon: FiCompass,
    name: 'Orders'
  }
]
