import axios from 'axios'
import { ApiData, ApiError } from '../interfaces/common'

const BASEURL = process.env.REACT_APP_API_BASEURL

export class AccurateService {
  static async findMany (params: any = {}) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findById (id: number) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate/${id}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async updateById (id: number, payload: any) {
    try {
      const resp = await axios.patch(`${BASEURL}/accurate/${id}`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findManyDatabases (id: number) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate/${id}/x/databases`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findManyGlAccounts (id: number, q?: string, page?: number) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate/${id}/x/glaccounts?q=${q || ''}&page=${page || 1}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findManyWarehouses (id: number, q?: string, page?: number) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate/${id}/x/warehouses?q=${q || ''}&page=${page || 1}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findManyBranches (id: number, q?: string, page?: number) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate/${id}/x/branches?q=${q || ''}&page=${page || 1}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findManyCustomers (id: number, q?: string, page?: number) {
    try {
      const resp = await axios.get(`${BASEURL}/accurate/${id}/x/customers?q=${q || ''}&page=${page || 1}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}
