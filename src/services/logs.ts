import axios from 'axios'
import { ApiData, ApiError } from '../interfaces/common'

const BASEURL = process.env.REACT_APP_API_BASEURL

export class LogsService {
  static async findMany (params: any = {}) {
    try {
      const resp = await axios.get(`${BASEURL}/logs`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async retryById (id: number) {
    try {
      const resp = await axios.post(`${BASEURL}/logs/${id}/retry`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}
