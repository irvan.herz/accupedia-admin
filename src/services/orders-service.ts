import axios from 'axios'
import { ApiData, ApiError } from 'interfaces/common'
import { API_BASE_URL } from 'libs/variables'

export class OrdersService {
  static async create (payload: any) {
    try {
      const resp = await axios.post(`${API_BASE_URL}/orders`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findMany (params: any = {}) {
    try {
      const resp = await axios.get(`${API_BASE_URL}/orders`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async updateById (id: number, payload: any) {
    try {
      const resp = await axios.patch(`${API_BASE_URL}/orders/${id}`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async deleteById (id: number) {
    try {
      const resp = await axios.delete(`${API_BASE_URL}/orders/${id}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}
