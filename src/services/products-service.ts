import axios from 'axios'
import { ApiData, ApiError } from 'interfaces/common'
import { API_BASE_URL } from 'libs/variables'

export class ProductCategoriesService {
  static async create (payload: any) {
    try {
      const resp = await axios.post(`${API_BASE_URL}/products/categories`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findMany (params: any = {}) {
    try {
      const resp = await axios.get(`${API_BASE_URL}/products/categories`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async updateById (id: number, payload: any) {
    try {
      const resp = await axios.patch(`${API_BASE_URL}/products/categories/${id}`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async deleteById (id: number) {
    try {
      const resp = await axios.delete(`${API_BASE_URL}/products/categories/${id}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}

export class ProductsService {
  static Categories = ProductCategoriesService

  static async create (payload: any) {
    try {
      const resp = await axios.post(`${API_BASE_URL}/products`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findMany (params: any = {}) {
    try {
      const resp = await axios.get(`${API_BASE_URL}/products`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async updateById (id: number, payload: any) {
    try {
      const resp = await axios.patch(`${API_BASE_URL}/products/${id}`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async deleteById (id: number) {
    try {
      const resp = await axios.delete(`${API_BASE_URL}/products/${id}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}
