import axios from 'axios'
import { ApiData, ApiError } from '../interfaces/common'

const BASEURL = process.env.REACT_APP_API_BASEURL

export class TokopediaService {
  static async create (payload: any) {
    try {
      const resp = await axios.post(`${BASEURL}/tokopedia`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findMany (params: any = {}) {
    try {
      const resp = await axios.get(`${BASEURL}/tokopedia`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async updateById (id: number, payload: any) {
    try {
      const resp = await axios.patch(`${BASEURL}/tokopedia/${id}`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}
