import { ApiData, ApiError } from 'interfaces/common'
import { axiosInstance } from 'libs/api'
const BASEURL = process.env.REACT_APP_API_BASEURL

export class UsersService {
  static async create (payload: any) {
    try {
      const resp = await axiosInstance.post(`${BASEURL}/users`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findMany (params: any = {}) {
    try {
      const resp = await axiosInstance.get(`${BASEURL}/users`, { params })
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findById (id: number) {
    try {
      const resp = await axiosInstance.get(`${BASEURL}/users/${id}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findByUsername (username: string) {
    try {
      const resp = await axiosInstance.get(`${BASEURL}/users/username/${username}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async findContextById (id: number) {
    try {
      const resp = await axiosInstance.get(`${BASEURL}/users/${id}/context`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async followById (id: number) {
    try {
      const resp = await axiosInstance.post(`${BASEURL}/users/${id}/follow`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async unfollowById (id: number) {
    try {
      const resp = await axiosInstance.delete(`${BASEURL}/users/${id}/unfollow`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async updateById (id: number, payload: any) {
    try {
      const resp = await axiosInstance.patch(`${BASEURL}/users/${id}`, payload)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }

  static async deleteById (id: number) {
    try {
      const resp = await axiosInstance.delete(`${BASEURL}/users/${id}`)
      return ApiData.fromResponse(resp)
    } catch (err: any) {
      throw new ApiError(err)
    }
  }
}
